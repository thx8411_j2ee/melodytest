package fr.fam.production.melodytest;


// IMPORTS
import java.util.logging.Logger;

//import javax.naming.Context; // Conflicts with javax.rs.core.contex, full qualified name used
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.annotation.security.RolesAllowed;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

// MAIN AND UNIQUE CLASS
@Path("/")
public class melodytest {

    // logger settings
    private static final Logger LOGGER =
        Logger.getLogger("com.fam.production.melody-test");

    // Greeting message
    private final String greeting = "<HTML><HEADER></HEADER><BODY><H1>melody-test</H1></BODY></HTML>";

    // Root path entry point, with greeting message
    @GET
    @Path("/")
    public final Response getIndex() {
        return (Response.ok().entity(greeting).build());
    }
}
